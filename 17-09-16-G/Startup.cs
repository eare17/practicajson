﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_17_09_16_G.Startup))]
namespace _17_09_16_G
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
