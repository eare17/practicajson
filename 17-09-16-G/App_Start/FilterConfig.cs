﻿using System.Web;
using System.Web.Mvc;

namespace _17_09_16_G
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
