﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _17_09_16_G.Models
{
    [Table("users")]
    public class Users
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre")]
        [StringLength(30, ErrorMessage = "El nombre debe contener menos de 30 caracteres.")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [StringLength(30, ErrorMessage = "El Apellido debe contener menos de 30 caracteres.")]
        [Display(Name = "Apellido")]
        public string Lastname { get; set; }
    }
}